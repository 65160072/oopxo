package tt.oop;

import java.util.Scanner;

public class Game {
    private Player P1;
    private Player P2;
    private Table table;

    public Game() {
        this.P1 = new Player('X');
        this.P2 = new Player('O');
        this.table = new Table(P1, P2); // Initialize the table here
    }
    public void play() {
        showWelcome();
        while (true) {
            showTable();
            showTurn();
            inputRowCol();
            if (table.checkWin()) {
                showTable();
                newGame();
                char winnerSymbol = table.getCurrentplayer().getSymbol();
                printWin(winnerSymbol);
                if (!askForRestart()) {
                    break; 
                }
            }
            if (table.checkDraw()) {
                newGame();
                printDraw();
                break;
            }
            table.switchPlayer();
        }
        newGame();
    }
    private void showWelcome() {
        System.out.println("Welcome to XO Game");
    }
    public static void printWin(char winnerSymbol) {
        System.out.println(winnerSymbol+" Win!!!");
    }
    private static void printDraw() {
        System.out.println("Draw");
    }
    private void showTable() {
        char[][] t = table.getTable();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(t[i][j] + " ");
            }
            System.out.println("");
        }
    }
    private void showTurn() {
        System.out.println(table.getCurrentplayer().getSymbol() + " Turn");
    }
    private void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input Row Col:");
        int row = sc.nextInt();
        int col = sc.nextInt();
        table.setRowCol(row, col);
    }  
    private void newGame() {
        table = new Table(P1, P2);
    }
    private boolean askForRestart() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Play again? (YES/NO): ");
        String answer = sc.next();
        return answer.equalsIgnoreCase("YES");
    }
}
