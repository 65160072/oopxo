package tt.oop;

import java.util.Scanner;

public class Table {
    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player P1;
    private Player P2;
    private Player currentplayer;
    static int row, col;
    
    public Table(Player P1, Player P2) {
        this.P1 = P1;
        this.P2 = P2;
        this.currentplayer = P1;
    }
    public Player getCurrentplayer() {
        return currentplayer;
    }
    public char[][] getTable() {
        return table;
    }
    public boolean setRowCol(int row, int col) {
        if (table[row - 1][col - 1] == '-') {
            table[row - 1][col - 1] = currentplayer.getSymbol();
            return true;
        }
        return false;
    }
    public void switchPlayer() {
        if (currentplayer == P1) {
            currentplayer = P2;
        } else {
            currentplayer = P1;
        }
    }
    public boolean checkWin() {
        if(checkRow()){
            return true;
        }
        if(checkCol()){
            return true;
        }if(checkDownleft()){
            return true;
        }if(checkDownright()){
            return true;
        }
        return false;   
    }
    public boolean checkRow() {
        char symbol = currentplayer.getSymbol();
        for (int i = 0; i < 3; i++) {
            if (table[row ][i] != symbol) {
                return false;
            }
        }
        return true;
    }
    private boolean checkCol() {
        char symbol = currentplayer.getSymbol();
        for (int j = 0; j < 3; j++) {
            if (table[j][col] != symbol) {
                return false;
            }
        }
        return true;
    }
    private  boolean checkDownleft() {
        char symbol = currentplayer.getSymbol();
        for(int i=0 ;i<3;i++){
            if(table[i][i]!=symbol){
                return false;
            }
        }
        return true;
    }   
    private boolean checkDownright() {
        char symbol = currentplayer.getSymbol();
        for(int i=0 ;i<3;i++){
            if(table[i][2-i]!=symbol){
                return false;
            }
        }
        return true;
    }  
    boolean checkDraw() {
         for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }
    private void printRestart() {
        System.out.println("Restart(YES/NO)");
        Scanner kb = new Scanner(System.in);
        String ans = kb.next();
        if(ans.equals("YES")){
            for(int i = 0;i<3;i++){
                for(int j =0;j<3;j++){
                    table[i][j] = '-';
                }
           }
        }
    }
}
 